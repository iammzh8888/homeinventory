﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using HomeInventory.Helper;
using HomeInventory.Models;
using Microsoft.Ajax.Utilities;

namespace HomeInventory.Controllers
{
    public class InventoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: HomeItems
        public ActionResult Index(string sortOrder, string sortDir, string searchInput)
        {
            ViewBag.searchInput = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(sortDir) && string.IsNullOrEmpty(searchInput))
            {
                var homeItems1 = db.HomeItems.Include(h => h.PurchaseInfo);
                return View(homeItems1.ToList());
            }

            var homeItems = db.HomeItems.AsQueryable().Include(h => h.PurchaseInfo);
            if (!string.IsNullOrEmpty(searchInput))
            {
                homeItems = homeItems.Where(h => h.Description.Contains(searchInput));
            }

            if (sortOrder != null)
            {
                switch (sortOrder.ToLower())
                {
                    case "description":
                        if (sortDir.ToLower() == "desc")
                        {
                            homeItems = homeItems.OrderByDescending(h => h.Description);
                        }
                        else
                        {
                            homeItems = homeItems.OrderBy(h => h.Description);
                        }
                        break;
                    case "location":
                        if (sortDir.ToLower() == "desc")
                        {
                            homeItems = homeItems.OrderByDescending(h => h.LocationId);
                        }
                        else
                        {
                            homeItems = homeItems.OrderBy(h => h.LocationId);
                        }
                        break;
                    case "model":
                        if (sortDir.ToLower() == "desc")
                        {
                            homeItems = homeItems.OrderByDescending(h => h.Model);
                        }
                        else
                        {
                            homeItems = homeItems.OrderBy(h => h.Model);
                        }
                        break;
                    case "serial number":
                        if (sortDir.ToLower() == "desc")
                        {
                            homeItems = homeItems.OrderByDescending(h => h.SerialNumber);
                        }
                        else
                        {
                            homeItems = homeItems.OrderBy(h => h.SerialNumber);
                        }
                        break;
                    default:
                        break;
                }
            }
            return View(homeItems.ToList());
        }

        // GET: HomeItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var homeItems = db.HomeItems.Include(c => c.PurchaseInfo);
            HomeItem homeItem = homeItems.FirstOrDefault(h => h.HomeItemId == id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            return View(homeItem);
        }

        // GET: HomeItems/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HomeItemId,Description,Model,SerialNumber,LocationId,PurchaseInfoId,PurchaseInfo.When,PurchaseInfo.Where,PurchaseInfo.Warranty,PurchaseInfo.Price,Photo,PhotoDB,PurchaseInfo")] HomeItemViewModel homeItemVM)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<HomeItemViewModel, HomeItem>().ForMember(h => h.Photo, opt => opt.Ignore());
                var homeItem = Mapper.Map<HomeItem>(homeItemVM);
                if (homeItemVM.Photo != null)
                {
                    homeItem.Photo = ImageConverter.ByteArrayFromPostedFile(homeItemVM.Photo);
                }

                db.PurchaseInfos.Add(homeItem.PurchaseInfo);
                db.HomeItems.Add(homeItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(homeItemVM);
        }

        // GET: HomeItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var homeItems = db.HomeItems.Include(c => c.PurchaseInfo);
            HomeItem homeItem = homeItems.FirstOrDefault(h => h.HomeItemId == id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            Mapper.CreateMap<HomeItem, HomeItemViewModel>().ForMember(h => h.Photo, opt => opt.Ignore());
            HomeItemViewModel hvm = Mapper.Map<HomeItemViewModel>(homeItem);
            hvm.PhotoDB = homeItem.Photo;
            return View(hvm);
        }

        // POST: HomeItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HomeItemId,Description,Model,SerialNumber,LocationId,PurchaseInfoId,PurchaseInfo.When,PurchaseInfo.Where,PurchaseInfo.Warranty,PurchaseInfo.Price,Photo,PhotoDB,PurchaseInfo")] HomeItemViewModel homeItemVM)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<HomeItemViewModel, HomeItem>().ForMember(f => f.Photo, opt => opt.Ignore());
                var homeItem = Mapper.Map<HomeItem>(homeItemVM);
                homeItem.Photo = homeItemVM.Photo != null ? ImageConverter.ByteArrayFromPostedFile(homeItemVM.Photo) : homeItemVM.PhotoDB;
                db.PurchaseInfos.AddOrUpdate(homeItem.PurchaseInfo);
                db.HomeItems.AddOrUpdate(homeItem);
                //db.Entry(homeItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(homeItemVM);
        }

        // GET: HomeItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var homeItems = db.HomeItems.Include(h=>h.PurchaseInfo);
            HomeItem homeItem = homeItems.FirstOrDefault(h => h.HomeItemId == id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            return View(homeItem);
        }

        // POST: HomeItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var homeItems = db.HomeItems.Include(h => h.PurchaseInfo);
            HomeItem homeItem = homeItems.FirstOrDefault(h => h.HomeItemId == id);
            db.PurchaseInfos.Remove(homeItem.PurchaseInfo);
            db.HomeItems.Remove(homeItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
