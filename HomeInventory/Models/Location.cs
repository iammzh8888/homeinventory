﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public enum Location
    {
        BedRoom,
        LivingRoom,
        KitchenRoom,
        BathRoom,
        OfficeRoom,
        Outside,
        Flower
    }
}