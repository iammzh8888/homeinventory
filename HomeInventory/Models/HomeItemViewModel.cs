﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class HomeItemViewModel
    {
        [Key]
        public int HomeItemId { get; set; }
        [StringLength(100)]
        public string Model { get; set; }
        [StringLength(100)]
        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }
        [Required]
        [Display(Name = "Location")]
        public Location LocationId { get; set; }
        [Required]
        [Display(Name = "Purchase Info")]
        public int PurchaseInfoId { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public byte[] PhotoDB { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public PurchaseInfo PurchaseInfo { get; set; }
    }
}